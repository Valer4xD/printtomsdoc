﻿using Microsoft.Office.Interop.Word;

namespace PrintToMsDoc
{
	public class MsDocTableHelper
	{
		/// <summary>
		/// Добавляет строку в таблицу.
		/// </summary>
		public void AddRowTable(_Document doc, int tableNumber, object BeforeRow = null)
		{
			if (null == BeforeRow)
				doc.Tables[tableNumber].Rows.Add();
			else
				doc.Tables[tableNumber].Rows.Add(BeforeRow);
		}
		public void EnlargeTable(_Document doc, int tableNumber, int numberOfAdding)
		{
			for (int i = 0; i < numberOfAdding; ++i)
			{
				doc.Tables[tableNumber].Range.Select();

				if (0 == i)
					doc.Tables[tableNumber].Range.Copy();

				doc.ActiveWindow.Selection.Paste();
			}
		}

		public void CopyPaste(Range targetRange, Range sourceRange)
		{
			sourceRange.Copy();
			targetRange.PasteAndFormat(WdRecoveryType.wdTableOverwriteCells);
		}
		public Range GetRange(_Document doc, int tableNumber, int row, int column) => doc.Tables[tableNumber].Cell(row, column).Range;

		public string GetText(_Document doc, int tableNumber, int row, int column) => GetRange(doc, tableNumber, row, column).Text;

		/// <summary>
		/// Выводит текст в заданную ячейку таблицы.
		/// </summary>
		public void SetText(_Document doc, int tableNumber, int row, int column, string text) => doc.Tables[tableNumber].Cell(row, column).Range.Text = text;
		public void SetText(Range range, int tableNumber, int row, int column, string text) => range.Tables[tableNumber].Cell(row, column).Range.Text = text;
	}
}