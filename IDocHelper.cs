﻿using Aspose.Words;
using Aspose.Words.Tables;

namespace CommonHelpers.DocBuilder.Interfaces
{
    public interface IDocHelper
    {
        Document GetDoc(string typeReport, string templatesDirectory = "document_templates", bool useOnlyTemplatesDirectory = false);

        void SetBookmark(Document doc, string bookmarkName, string text);

        void CopyPaste(Document docCopy, Document docPaste, SectionStart sectionStart = SectionStart.Continuous, bool inEnd = true);

        void AddRowTable(Document doc, int tableNumber, int? copyRowNumber = null);
        void InsertRowTable(Document doc, int tableNumber, int copyRowNumber, int pasteRowNumber);

        void IncreaseTableWithCopiesOfThisTable(Table table, int numberOfAddingTables);
        void CopyPaste(Document srcDoc, Document dstDoc, Cell cellCopy, ref Cell cellPaste);
        Table GetTable(Document doc, CellAddress[] cellAddressArray, int tableId);
        Cell GetCell(Table table, int rowId, int columnId);

        Cell GetCell(Document doc, int tableNumber, int row, int column);

        void SetText(Document doc, Cell cell, string text);
        void SetText(Document doc, int tableNumber, int row, int column, string text);
        void SetText(Document doc, int pageNumber, HeaderFooter headerFooter, int tableNumber, int row, int column, string text);

        string GetText(Document doc, int tableNumber, int row, int column);

        HeaderFooter GetHeaderFooter(Section section, int pageNumber);

        int GetRowsCount(Document doc);
        int GetColumnsCount(Document doc, int row);
    }
}
