﻿namespace CommonHelpers.DocBuilder
{
	public class CellAddress
	{
		public int TableId;
		public int RowId;
		public int ColumnId;

		public CellAddress(int tableId, int rowId, int columnId)
		{
			TableId = tableId;
			RowId = rowId;
			ColumnId = columnId;
		}
	}
}
