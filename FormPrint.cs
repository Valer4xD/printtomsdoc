﻿using Microsoft.Office.Interop.Word;
using PrintToMsDoc.HelpersToControls;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Application = Microsoft.Office.Interop.Word.Application;
using ParagraphSettings = PrintToMsDoc.MsDocHelper.ParagraphSettings;

namespace PrintToMsDoc
{
	internal partial class FormPrint : Form
	{
		internal FormPrint()
		{
			InitializeComponent();

			var text = new StringBuilder();

			text.Append("Я помню чудное мгновенье:\r\n");
			text.Append("Передо мной явилась ты,\r\n");
			text.Append("Как мимолетное виденье,\r\n");
			text.Append("Как гений чистой красоты.\r\n");
			text.Append("\r\n");
			text.Append("В томленьях грусти безнадежной,\r\n");
			text.Append("В тревогах шумной суеты,\r\n");
			text.Append("Звучал мне долго голос нежный\r\n");
			text.Append("И снились милые черты.\r\n");
			text.Append("\r\n");
			text.Append("Шли годы. Бурь порыв мятежный\r\n");
			text.Append("Рассеял прежние мечты,\r\n");
			text.Append("И я забыл твой голос нежный,\r\n");
			text.Append("Твои небесные черты.");

			textBoxPrint.Text = text.ToString();
		}

		private void buttonPrint_Click(object sender, EventArgs e)
		{
			var wordHelper = new MsDocHelper();

			_Application word = new Application();
			_Document doc = wordHelper.GetDoc(@"Бланки\Документ.dot", ref word);

			try
			{
				var paragraphsSettings = new List<ParagraphSettings>
				{
					new ParagraphSettings(bookmarkName: "DEBUG", countLines: 3)
				};

				foreach (ParagraphSettings paragraphSettings in paragraphsSettings)
					wordHelper.SetBookmark(
						word,
						doc,
						paragraphSettings.BookmarkName,
						string.Empty,
						needFitText: true,
						fontSize: 0.0F,
							debug: true,
							paragraphSettings.CountLines);

				string text = $"{textBoxPrint.Text.Replace("\r", string.Empty).Replace("\n", CommonStrings.SpaceString)}";
				var pixelsInLines3 = new int[] { 632, 632, 632 };
				var pixelsInLines2 = new int[] { 632, 632 };

				string bookmarkName = "TEXTIN";
				wordHelper.SetParagraph(
					word,
					doc,
					bookmarkName,
					new ParagraphParams($"{wordHelper.GetBookmarkText(word, doc, $"{bookmarkName}1")}" + text),
					countLines: 3,
					pixelsInLines3);



				wordHelper.SetParagraph(
					word,
					doc,
					bookmarkName: "TEXTOUTFIT",
					new ParagraphParams(text),
					countLines: 3/*,
					startLens: new int[] { 77, 77, 77 }*/);

				wordHelper.SetParagraph(
					word,
					doc,
					bookmarkName: "TEXTOUTFONT",
					new ParagraphParams(text, cropText: false),
					countLines: 3,
					fitText: false,
					isTable: false,
					pickUpFontSize: true);


				wordHelper.SetParagraph(
					word,
					doc,
					bookmarkName: "ONEBOOKMARKFIT",
					new ParagraphParams(text, cropText: false),
					countLines: 1,
					pixelsInLines: new int[] { 632 });

				wordHelper.SetParagraph(
					word,
					doc,
					bookmarkName: "ONEBOOKMARKFONT",
					new ParagraphParams(text, cropText: false),
					countLines: 1,
					pixelsInLines: new int[] { 632 },
					fitText: false,
					isTable: false,
					pickUpFontSize: true);



				wordHelper.SetParagraph(
					word,
					doc,
					bookmarkName: "TABLEFITCROP",
					new ParagraphParams(text),
					countLines: 3,
					pixelsInLines3);

				wordHelper.SetParagraph(
					word,
					doc,
					bookmarkName: "TABLEFONTCROP",
					new ParagraphParams(text),
					countLines: 3,
					pixelsInLines3,
					fitText: false,
					pickUpFontSize: true);

				wordHelper.SetParagraph(
					word,
					doc,
					bookmarkName: "TABLEFIT",
					new ParagraphParams(text, cropText: false),
					countLines: 2,
					pixelsInLines2);

				wordHelper.SetParagraph(
					word,
					doc,
					bookmarkName: "TABLEFONT",
					new ParagraphParams(text, cropText: false),
					countLines: 2,
					pixelsInLines2,
					fitText: false,
					pickUpFontSize: true);



				wordHelper.SetParagraph(
					word,
					doc,
					bookmarkName: "FONTCROP",
					new ParagraphParams(text),
					countLines: 3,
					pixelsInLines3,
					fitText: false,
					isTable: false,
					pickUpFontSize: true);

				wordHelper.SetParagraph(
					word,
					doc,
					bookmarkName: "FONT",
					new ParagraphParams(text, cropText: false),
					countLines: 2,
					pixelsInLines2,
					fitText: false,
					isTable: false,
					pickUpFontSize: true);

				word.Visible = true;
			}
			catch (Exception ex)
			{
				new MessageBoxHelper().ShowErrorMessage($"Ошибка при заполнении документа. Подробности: {ex.Message}");
				wordHelper.ReleaseMSWord(word);
			}
		}
	}
}
