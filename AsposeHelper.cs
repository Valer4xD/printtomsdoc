﻿using Aspose.Words;
using Aspose.Words.Tables;
using CommonHelpers.DocBuilder.Interfaces;
using System;
using System.IO;
using System.Reflection;

namespace CommonHelpers.DocBuilder
{
	public class AsposeHelper : IDocHelper
	{
		public Document GetDoc(string typeReport, string templatesDirectory = "document_templates", bool useOnlyTemplatesDirectory = false)
		{
			string filePath = $@"{(
				! useOnlyTemplatesDirectory ?
					// AppDomain.CurrentDomain.BaseDirectory : // Папка с .exe.
					(Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath) + @"\") :
					string.Empty)
						}{templatesDirectory}\{typeReport}";

			try
			{
				return new Document(filePath);
			}
			catch (Exception ex)
			{
				throw new Exception($@"Не удалось загрузить шаблон документа {filePath}{'\n'}{ex.Message}");
			}
		}
		
		public void SetBookmark(Document doc, string bookmarkName, string text)
		{
			DocumentBuilder documentBuilder = new DocumentBuilder(doc);
			if (documentBuilder.MoveToBookmark(bookmarkName))
			{
				Node bookmarkNode = documentBuilder.CurrentNode;
				bookmarkNode.NextSibling.Remove();
				bookmarkNode.Remove();
				documentBuilder.Write(text);
			}
		}
		
		public void CopyPaste(Document docCopy, Document docPaste, SectionStart sectionStart = SectionStart.Continuous, bool inEnd = true)
		{
			docCopy.FirstSection.PageSetup.SectionStart = sectionStart;

			DocumentBuilder builder = new DocumentBuilder(docPaste);
			if (inEnd)
				builder.MoveToDocumentEnd();
			/*
			if (fromNewPage)
				builder.InsertBreak(BreakType.PageBreak);
			*/

			docPaste.AppendDocument(docCopy, ImportFormatMode.KeepSourceFormatting);
		}

		public void AddRowTable(Document doc, int tableId, int? copyRowNumber = null)
		{
			TableCollection tables = doc.FirstSection.Body.Tables;
			if (null == copyRowNumber)
				copyRowNumber = tables[tableId - 1].Rows.Count - 1;
			Node cloneLastRow = tables[tableId - 1].Rows[copyRowNumber.Value].Clone(true);
			tables[tableId - 1].Rows.Add(cloneLastRow);
		}
		public void InsertRowTable(Document doc, int tableId, int copyRowNumber, int pasteRowNumber)
		{
			TableCollection tables = doc.FirstSection.Body.Tables;
			Node cloneLastRow = tables[tableId - 1].Rows[copyRowNumber - 1].Clone(true);
			tables[tableId - 1].Rows.Insert(pasteRowNumber - 1, cloneLastRow);
		}

		public void IncreaseTableWithCopiesOfThisTable(Table table, int numberOfAddingTables)
		{
			int r;
			int rowsCount = table.Rows.Count;

			for (int t = 0; t < numberOfAddingTables; ++t)
				for (r = 0; r < rowsCount; ++r)
				{
					Row cloneRow = (Row)(table.Rows[r].Clone(true));
					table.Rows.Add(cloneRow);
				}
		}
		public void CopyPaste(Document srcDoc, Document dstDoc, Cell cellCopy, ref Cell cellPaste)
		{
			NodeImporter nodeImporter = new NodeImporter(srcDoc, dstDoc, ImportFormatMode.KeepSourceFormatting);
			Node impNode = nodeImporter.ImportNode(cellCopy, true);
			cellPaste.ParentNode.InsertAfter(impNode, cellPaste);
			cellPaste.ParentNode.RemoveChild(cellPaste);
		}
		public Table GetTable(Document doc, CellAddress[] cellAddressArray, int tableId)
		{
			Table result;
			TableCollection tableCollection = doc.FirstSection.Body.Tables;

			if(null != cellAddressArray)
				foreach(CellAddress cellAddress in cellAddressArray)
					tableCollection = tableCollection[cellAddress.TableId - 1].Rows[cellAddress.RowId - 1].Cells[cellAddress.ColumnId - 1].Tables;

			result = tableCollection[tableId - 1];

			return result;
		}
		public Cell GetCell(Table table, int rowId, int columnId) => table.Rows[rowId - 1].Cells[columnId - 1];

		public Cell GetCell(Document doc, int tableNumber, int row, int column) => doc.FirstSection.Body.Tables[tableNumber - 1].Rows[row - 1].Cells[column - 1];

		public void SetText(Document doc, Cell cell, string text)
		{
			DocumentBuilder documentBuilder = new DocumentBuilder(doc);
			documentBuilder.MoveTo(cell.FirstParagraph);
			documentBuilder.Write(text);
		}

		public void SetText(Document doc, int tableId, int rowId, int columnId, string text)
		{
			DocumentBuilder documentBuilder = new DocumentBuilder(doc);
			documentBuilder.MoveToCell(tableId - 1, rowId - 1, columnId - 1, 0);
			documentBuilder.Write(text);
		}
		public void SetText(Document doc, int pageNumber, HeaderFooter headerFooter, int tableId, int rowId, int columnId, string text)
		{
			if (0 == headerFooter.Tables.Count) return; // Это костыль от триальной версии Aspoose. Она обрезает документ и по всей видимости таблицы убирает перед удалением страниц.

			DocumentBuilder documentBuilder = new DocumentBuilder(doc);
			documentBuilder.MoveToSection(pageNumber);
			documentBuilder.MoveToHeaderFooter(headerFooter.HeaderFooterType);
			documentBuilder.MoveToCell(tableId - 1, rowId - 1, columnId - 1, 0);
			documentBuilder.Write(text);
		}

		public string GetText(Document doc, int tableNumber, int row, int column) => doc.FirstSection.Body.Tables[tableNumber - 1].Rows[row - 1].Cells[column - 1].GetText();

		public HeaderFooter GetHeaderFooter(Section section, int pageNumber)
		{
			if(section.PageSetup.DifferentFirstPageHeaderFooter)
				if (0 == pageNumber)
					return section.HeadersFooters[HeaderFooterType.HeaderFirst];

			if(section.PageSetup.OddAndEvenPagesHeaderFooter)
				if(0 != pageNumber % 2)
					return section.HeadersFooters[HeaderFooterType.HeaderPrimary];
				else
					return section.HeadersFooters[HeaderFooterType.HeaderEven];

			return section.HeadersFooters[HeaderFooterType.HeaderPrimary];
		}

		public int GetRowsCount(Document doc) => doc.FirstSection.Body.Tables[0].Rows.Count;
		public int GetColumnsCount(Document doc, int rowId) => doc.FirstSection.Body.Tables[0].Rows[rowId - 1].Cells.Count;
	}
}
